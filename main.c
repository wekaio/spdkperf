#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <rte_eal.h>
#include <rte_errno.h>
#include "nvme/nvme_internal.h"

#define logx_nonl(c, fmt, args...) printf("%-20.20s:%3u|" c "| " fmt, __FILE__, __LINE__, args)
#define logi(fmt, args...) logx_nonl("I", fmt "\n", args)
#define loge(fmt, args...) logx_nonl("E", fmt "\n", args)
#define logi0(s) logi("%s", s)

#define MARK() logi("MARK %s", __FUNCTION__)

#define TB1 "    "

#define CHKC(c, r) \
do \
{ \
    if (c) \
    { \
        loge("%s", "ERROR"); \
        return r; \
    } \
} \
while (0)
#define CHK(c) CHKC(c, c)
#define CHKCF(c) CHKC(c, ERR_FAIL)

#define ERR_OK 0
#define ERR_FAIL 1

#define NUM_CTRLR_ENTRIES 3

typedef struct
{
	char name[128];
	struct spdk_nvme_ctrlr* ctrlr;
	struct spdk_nvme_ns *ns;
    uint64_t num_sectors;
    uint32_t max_io_xfer_size;
    uint32_t sector_size;
}
ctrlr_entry_t;

typedef struct
{
    struct spdk_nvme_qpair* qpair;
    bool completed; 
}
io_complete_t;

static struct
{
    ctrlr_entry_t ctrlrs[NUM_CTRLR_ENTRIES];
    uint32_t ctrlrsl;
    struct
    {
        uint32_t read_size;
        uint32_t write_size;
        uint32_t count;
        bool log_trace;
    }
    conf;
}
db = {};

void SPDK_NOTICELOG_WEKA(const char *buf)
{
    logx_nonl("I", "%s", buf);
}
void SPDK_WARNLOG_WEKA(const char *buf)
{
    //logx_nonl ("W", "%s", buf);
}
void SPDK_ERRLOG_WEKA(const char *buf)
{
    logx_nonl("E", "%s", buf);
}
void SPDK_TRACELOG_WEKA(const char *buf)
{
    if (db.conf.log_trace)
    {
        logx_nonl("T", "%s", buf);
    }
}

static bool probe_cb(void *cb_ctx, const struct spdk_nvme_transport_id *trid, struct spdk_nvme_ctrlr_opts *opts)
{
    MARK();
	logi("Attaching to %s", trid->traddr);
    
	return trid->traddr[6] == '4' ? true : false;
}

int nvme_pcie_ctrlr_cfg_interrupts(struct spdk_nvme_ctrlr *ctrlr);
static void attach_cb(void *cb_ctx, const struct spdk_nvme_transport_id *trid, struct spdk_nvme_ctrlr *ctrlr, const struct spdk_nvme_ctrlr_opts *opts)
{
	int nsid, num_ns;
    ctrlr_entry_t* ce;
	struct spdk_nvme_ns *ns;
	const struct spdk_nvme_ctrlr_data *cdata;

    MARK();
    nvme_pcie_ctrlr_cfg_interrupts(ctrlr);
	logi("Attached to %s", trid->traddr);
    if (db.ctrlrsl >= NUM_CTRLR_ENTRIES)
    {
        return;
    }
    ce = &db.ctrlrs[db.ctrlrsl];

    ce->ctrlr = ctrlr;

	cdata = spdk_nvme_ctrlr_get_data(ctrlr);
    CHKC(!cdata, );
	logi("vid=%04x, ssvid=%04x", cdata->vid, cdata->ssvid);
	snprintf(ce->name, sizeof(ce->name), "%-20.20s (%-20.20s)", cdata->mn, cdata->sn);

	num_ns = spdk_nvme_ctrlr_get_num_ns(ctrlr);
    CHKC(!num_ns, );
	logi("Using controller '%s' with %d namespaces.", ce->name, num_ns);
    ce->ns = NULL;
	for (nsid = 1; nsid <= num_ns; nsid++)
    {
		ns = spdk_nvme_ctrlr_get_ns(ctrlr, nsid);
        if (!ns || !spdk_nvme_ns_is_active(ns))
        {
            continue;
        }

        ce->max_io_xfer_size = spdk_nvme_ns_get_max_io_xfer_size(ns);
        ce->sector_size = spdk_nvme_ns_get_sector_size(ns);
        ce->num_sectors = spdk_nvme_ns_get_num_sectors(ns);
        ce->ns = ns;

        logi("  Namespace ID: %d size: %juGB", spdk_nvme_ns_get_id(ns), spdk_nvme_ns_get_size(ns) / 1000000000);
        logi("max_io_xfer_size                =%u", ce->max_io_xfer_size);
        logi("sector_size                     =%u", ce->sector_size);
        logi("extended_sector_size            =%u", spdk_nvme_ns_get_extended_sector_size(ns));
        logi("num_sectors                     =%lu", ce->num_sectors);
        logi("size                            =%lu", spdk_nvme_ns_get_size(ns));
        logi("pi_type                         =%u", spdk_nvme_ns_get_pi_type(ns));
        logi("md_size                         =%u", spdk_nvme_ns_get_md_size(ns));
        logi("supports_extended_lba           =%u", spdk_nvme_ns_supports_extended_lba(ns));
        logi("dealloc_logical_block_read_value=%u", spdk_nvme_ns_get_dealloc_logical_block_read_value(ns));
        logi("optimal_io_boundary             =%u", spdk_nvme_ns_get_optimal_io_boundary(ns));
        logi("flags                           =%08x", spdk_nvme_ns_get_flags(ns));

        // saving only the first ns
        break;
	}
    CHKC(!ce->ns, );
    db.ctrlrsl++;
}

static void qpair_wait_for_completion(struct spdk_nvme_qpair* qpair, bool* completed, uint32_t* loops)
{
    uint32_t cnt = 0;
    while (!*completed)
    {
        cnt++;
        spdk_nvme_qpair_process_completions(qpair, 0);
    }
    if (loops)
    {
        *loops = cnt;
    }
}

static void complete_cb(void *arg, const struct spdk_nvme_cpl *completion)
{
    io_complete_t* ioc = arg;
	int rc;

	if (spdk_nvme_cpl_is_error(completion))
    {
		logi("I/O error status: 0x%04x", *(uint16_t*)&completion->status);
		logi0("Write I/O failed, aborting run");
		exit(1);
	}
    ioc->completed = true;
}

static void usage(const char *program_name)
{
	logi("%s [options]", program_name);
	logi0("options:");
	logi0(" -r   read_size");
	logi0(" -w   write_size");
	logi0(" -c   count, repeat operation");
	logi0(" -t   show log trace level");
}

static int parse_args(int argc, char **argv)
{
    int op;

    db.conf.read_size = 4096;
    db.conf.write_size = 0;
    db.conf.count = 1;
    db.conf.log_trace = true;
    while ((op = getopt(argc, argv, "c:r:w:t")) != -1)
    {
        switch (op)
        {
            case 'r':
                CHKCF(!optarg);
                db.conf.read_size = strtoul(optarg, NULL, 10);
                break;
            case 'w':
                CHKCF(!optarg);
                db.conf.write_size = strtoul(optarg, NULL, 10);
                break;
            case 'c':
                CHKCF(!optarg);
                db.conf.count = strtoul(optarg, NULL, 10);
                break;
            case 't':
                db.conf.log_trace = true;
                break;
            default:
                usage(argv[0]);
                return ERR_FAIL;
        }
    }
    logi0("db.conf");
    logi(TB1 "read_size  =%u", db.conf.read_size);
    logi(TB1 "write_size =%u", db.conf.write_size);
    logi(TB1 "count      =%u", db.conf.count);
    logi(TB1 "log_trace  =%u", db.conf.log_trace);

    return ERR_OK;
}

int main(int argc, char** argv)
{
    ctrlr_entry_t* ce;
	struct spdk_env_opts opts;
    int rc;

    rc = parse_args(argc, argv);
    CHK(rc);

	spdk_env_opts_init(&opts);
	opts.name = "spdkperf";
	opts.shm_id = 0;
	rc = spdk_env_init(&opts);
    CHK(rc);

    MARK();
    rc = spdk_nvme_probe(NULL, (void*)(uintptr_t)0x11223344, probe_cb, attach_cb, NULL);
    CHK(rc);

    logi("db.ctrlrsl=%u", db.ctrlrsl);
    CHKCF(!db.ctrlrsl);
    ce = &db.ctrlrs[0];
    {
        struct spdk_nvme_qpair* qpair;
        io_complete_t ioc;
        uint32_t lba_count;
        uint32_t i;
        uint32_t loop;
        uint32_t loops;
        void* buf;

        ioc.qpair = qpair;
        qpair = spdk_nvme_ctrlr_alloc_io_qpair(ce->ctrlr, NULL, 0);
        CHKCF(!qpair);

        if (db.conf.write_size)
        {
            lba_count = db.conf.write_size / ce->sector_size;
            logi("Write %s: %u lba_size=%u, lba_count=%u", ce->name, db.conf.write_size, ce->sector_size, lba_count);
        
            buf = spdk_dma_zmalloc(db.conf.write_size, ce->sector_size, NULL);
            CHKCF(!buf);
            memset(buf, 0xAA, db.conf.write_size);
            ioc.completed = false;
            rc = spdk_nvme_ns_cmd_write(ce->ns, qpair, buf, 0, lba_count, complete_cb, &ioc, 0);
            CHK(rc);
            qpair_wait_for_completion(qpair, &ioc.completed, &loops);
            spdk_dma_free(buf);
            logi("completion loops over %u ops are %u", db.conf.count, loops);
        }
        if (db.conf.read_size)
        {
            lba_count = db.conf.read_size / ce->sector_size;
            logi("Read %s: %u lba_size=%u, lba_count=%u", ce->name, db.conf.read_size, ce->sector_size, lba_count);

            buf = spdk_dma_zmalloc(db.conf.read_size, ce->sector_size, NULL);
            CHKCF(!buf);
            loops = 0;
            for (i = 0; i < db.conf.count; i++)
            {
                ioc.completed = false;
                rc = spdk_nvme_ns_cmd_read(ce->ns, qpair, buf, 0, lba_count, complete_cb, &ioc, 0);
                CHK(rc);
                qpair_wait_for_completion(qpair, &ioc.completed, &loop);
                loops += loop;
            }
            spdk_dma_free(buf);
            logi("completion loops over %u ops are %u", db.conf.count, loops);
        }

		rc = spdk_nvme_ctrlr_free_io_qpair(qpair);
        CHK(rc);
    }

    rc = spdk_nvme_detach(ce->ctrlr);
    CHK(rc);

    return 0;
}

