NAME = spdkperf

TRGT ?= x86_64-native-linuxapp-gcc
DPDKDIR ?= external/dpdk
SPDKDIR ?= external/spdk

CFLGS += -o $(NAME) -g -ggdb
INCS += \
	-I$(DPDKDIR)/$(TRGT)/include/dpdk \
	-I$(SPDKDIR)/include \
	-I$(SPDKDIR)/lib
LFLGS += \
	-L$(DPDKDIR)/$(TRGT)/lib \
	-L$(SPDKDIR)/build/lib
LFLGS += \
	-lspdk_nvme \
	-lspdk_env_dpdk \
	-lspdk_util \
	-lspdk_log \
	-lrte_eal \
	-lrte_kvargs \
	-lrte_mbuf \
	-lrte_mempool \
	-lrte_ring \
	-lrte_bus_pci \
	-lrte_pci \
	-luuid \
	-lnuma \
	-lpthread \
	-ldl

all:
	gcc main.c $(CFLGS) $(INCS) $(LFLGS)
clean:
	rm -rf $(NAME)

dpdk: $(DPDKDIR)/build/.config
	$(MAKE) -C $(DPDKDIR)
	$(MAKE) -C $(DPDKDIR) DESTDIR=$(TRGT) prefix= install
$(DPDKDIR)/build/.config: $(DPDKDIR)/config/defconfig_$(TRGT)
	echo $^
	$(MAKE) -C $(DPDKDIR) T=$(TRGT) config
	sed -i 's/CONFIG_RTE_EAL_WEKA_ENVIRONMENT=y/CONFIG_RTE_EAL_WEKA_ENVIRONMENT=n/' $(DPDKDIR)/build/.config
	sed -i 's/CONFIG_RTE_LIBRTE_MLX5_PMD=y/CONFIG_RTE_LIBRTE_MLX5_PMD=n/' $(DPDKDIR)/build/.config
	$(DPDKDIR)/buildtools/gen-config-h.sh $(DPDKDIR)/build/.config > $(DPDKDIR)/build/include/rte_config.h
dpdk_clean:
	rm -rf $(DPDKDIR)/build $(DPDKDIR)/$(TRGT)

spdk: $(SPDKDIR)/mk/config.mk
	$(MAKE) -C $(SPDKDIR)
$(SPDKDIR)/mk/config.mk: $(SPDKDIR)/CONFIG
	cd $(SPDKDIR) && ./configure --with-dpdk=$(abspath $(DPDKDIR)/$(TRGT))
spdk_clean:
	rm -rf $(SPDKDIR)/mk/config.mk

PCIADDR ?= 0000:84:00.0
rebind:
	echo $(PCIADDR) > /sys/bus/pci/drivers/uio_pci_generic/unbind
	echo $(PCIADDR) > /sys/bus/pci/drivers/nvme/bind
	echo $(PCIADDR) > /sys/bus/pci/drivers/nvme/unbind
	echo $(PCIADDR) > /sys/bus/pci/drivers/uio_pci_generic/bind

